//
//  Parameters.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol AnyParametersContainer: class {
    var anyParameters: Any { get set }
}

public protocol Parameterable: AnyParametersContainer {
    associatedtype Parameters: Appliable
    var parameters: Parameters { get set }
}

extension Parameterable {
    public var anyParameters: Any {
        get { return parameters }
        set {
            guard let newValue = newValue as? Parameters else { return }
            parameters = newValue
        }
    }
}

public protocol Appliable {
    func apply(to receiver: inout Any)
}

extension Appliable {
    public func apply(to receiver: inout Any) {}
}

extension Parameterable where Self: StackingActionPerformer {
    public func prepare(_ action: Action) {
        var action = action as Any
        parameters.apply(to: &action)
    }
    public func prepare(result: inout Any) {
        parameters.apply(to: &result)
    }
}

extension AnyParametersContainer {
    public func modifyParameters<T>(as type: T.Type, modification: (inout T) -> Void) {
        modify(&anyParameters, as: type, modification: modification)
    }
}

public func modify<T>(_ object: inout Any, as type: T.Type, modification: (inout T) -> Void) {
    guard var result = object as? T else { return }
    modification(&result)
    object = result
}
