//
//  Action.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

open class Action {
    public init() {}
    public var performers: [WeakStackingActionPerformer] = []
    
    open var isCancelled: Bool = false
    open func cancel() {
        isCancelled = true
        (self as? ParentAction)?.actions.forEach { $0.cancel() }
    }
    
    open func perform(completion: @escaping () -> Void) { completion() }
}

public protocol ParentAction: Action {
    var actions: [Action] { get }
}

extension ParentAction {
    public func setupChildren() {
        for child in actions {
            child.performers = performers
            (child as? ParentAction)?.setupChildren()
        }
    }
}

extension Action {
    public func performed(by performer: ActionPerformer) -> Self {
        performer.perform(self)
        return self
    }
}
