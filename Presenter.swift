//
//  Presenter.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol Presenter: AnyPresenter {
    associatedtype Input
    associatedtype Receiver
    /// Usually receiver should have weak reference, especially if it is view controller presented somewhere
    var receiver: Receiver? { get }
    func present(_ input: Input, by receiver: Receiver)
}

public protocol AnyPresenter {
    @discardableResult
    func present(_ input: Any) -> Bool
}

extension Presenter {
    @discardableResult
    public func present(_ input: Any) -> Bool {
        guard let input = input as? Input else { return false }
        guard let receiver = receiver else { return false }
        present(input, by: receiver)
        return true
    }
}

public protocol PresentersContainer {
    var presenters: [AnyPresenter] { get }
}

extension PresentersContainer {
    public var presenters: [AnyPresenter] {
        return Mirror(reflecting: self).children.compactMap { $0.value as? AnyPresenter }
    }
}

public protocol PassingResultsToPresenters: AnyResultReceiver, PresentersContainer {}

extension PassingResultsToPresenters {
    public func receive(result: Any) {
        presenters.forEach { $0.present(result) }
    }
}
