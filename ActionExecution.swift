//
//  ActionExecution.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

extension StackingActionPerformer {
    
    public func perform(_ action: Action) {
        action.performers = []
        performOnStack(action)
    }
    
    private func performOnStack(_ action: Action) {
        action.performers.insert(WeakStackingActionPerformer(self), at: 0)
        if let parent = actionPerformer {
            parent.performOnStack(action)
        } else {
            (action as? ParentAction)?.setupChildren()
            var actionInProgress: Action? = action
            actionInProgress?.prepareExecuteAndFinalise { actionInProgress = nil }
        }
    }
    
}

extension Action {
    
    public final func prepareExecuteAndFinalise(completion: @escaping () -> Void) {
        prepare()
        if isCancelled { return }
        perform { [weak self] in
            self?.finalise()
            completion()
        }
    }
    
    public func prepare() {
        let performers = self.performers.compactMap { $0.performer }
        for performer in performers {
            if isCancelled { return }
            performer.prepare(self)
        }
        for performer in performers.reversed() {
            if isCancelled { return }
            performer.trackWillPerform(self)
        }
    }
    
    public func finalise() {
        let performers = self.performers.compactMap { $0.performer }
        for performer in performers {
            if isCancelled { return }
            performer.trackDidPerform(self)
        }
        if isCancelled { return }
        if var result = (self as? AnyResultContainer)?.anyResult {
            for performer in performers {
                performer.prepare(result: &result)
            }
            for performer in performers {
                (performer as? AnyResultReceiver)?.receive(result: result)
            }
        }
    }
    
}
