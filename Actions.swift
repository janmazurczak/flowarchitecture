//
//  Actions.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation

public extension Action {
    
    static func wait(_ duration: TimeInterval) -> WaitAction {
        return WaitAction(duration)
    }
    
    static func custom(_ block: @escaping (_ completion: @escaping () -> Void) -> Void) -> CustomAction {
        return CustomAction(block)
    }
    
    func with(timeout: TimeInterval, action timeoutAction: Action) -> ActionsSequence {
        let waitAction = WaitAction(timeout)
        return .sequence([
            .alternative([
                self,
                waitAction,
                ]),
            CustomAction({ [weak waitAction] completion in
                if waitAction?.isCancelled ?? false {
                    timeoutAction.cancel()
                }
                completion()
            }),
            timeoutAction
            ])
    }
    
}

open class WaitAction: Action {
    private let duration: TimeInterval
    public init(_ duration: TimeInterval) {
        self.duration = duration
        super.init()
    }
    open override func perform(completion: @escaping () -> Void) {
        let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(duration * 1000))
        (OperationQueue.current?.underlyingQueue ?? .main).asyncAfter(deadline: deadline) {
            completion()
        }
    }
}

open class CustomAction: Action {
    let block: (_ completion: @escaping () -> Void) -> Void
    public init(_ performingBlock: @escaping (_ completion: @escaping () -> Void) -> Void) {
        self.block = performingBlock
        super.init()
    }
    open override func perform(completion: @escaping () -> Void) {
        block(completion)
    }
}
