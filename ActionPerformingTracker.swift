//
//  ActionPerformingTracker.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol ActionPerformingTracker: class {
    func willPerform(_ action: Action)
    func didPerform(_ action: Action)
    var actionPerformingTrackers: [ActionPerformingTracker] { get }
}

extension ActionPerformingTracker {
    public func willPerform(_ action: Action) {}
    public func didPerform(_ action: Action) {}
    public var actionPerformingTrackers: [ActionPerformingTracker] { return [] }
}

extension ActionPerformingTracker {
    public func trackWillPerform(_ action: Action, on stack: [ActionPerformingTracker] = []) {
        if stack.contains(where: { $0 === self }) { return }
        willPerform(action)
        for tracker in actionPerformingTrackers {
            tracker.trackWillPerform(action, on: stack + [self])
        }
    }
    public func trackDidPerform(_ action: Action, on stack: [ActionPerformingTracker] = []) {
        if stack.contains(where: { $0 === self }) { return }
        didPerform(action)
        for tracker in actionPerformingTrackers {
            tracker.trackDidPerform(action, on: stack + [self])
        }
    }
}
