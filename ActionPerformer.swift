//
//  ActionPerformer.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol ActionPerformer: class {
    func perform(_ action: Action)
}

extension ActionPerformer {
    public func performed<A>(_ action: A) -> A where A: Action {
        perform(action)
        return action
    }
}

public protocol StackingActionPerformer: ActionPerformer, ActionPerformingTracker {
    func prepare(_ action: Action)
    func prepare(result: inout Any)
    var actionPerformer: StackingActionPerformer? { get }
}

public struct WeakStackingActionPerformer {
    public init(_ performer: StackingActionPerformer) { self.performer = performer }
    public weak var performer: StackingActionPerformer?
}

public protocol ActionPerformerCaller: class {
    /// keep it weak
    var actionPerformer: ActionPerformer? { get set }
}
