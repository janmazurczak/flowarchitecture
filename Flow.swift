//
//  Flow.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol Flow: StackingActionPerformer, PassingResultsToPresenters, FlowChild {
    var parent: Flow? { get set }
    init()
}

extension Flow {
    public var actionPerformer: StackingActionPerformer? { return parent }
    public var actionPerformingTrackers: [ActionPerformingTracker] {
        return Mirror(reflecting: self).children
            .compactMap { $0.value as? ActionPerformingTracker }
            .filter { !($0 is Flow) }
    }
}
