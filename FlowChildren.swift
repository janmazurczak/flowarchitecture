//
//  FlowChildren.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol SansArgumentInitializable {
    init()
}

public protocol FlowChild: class {
    func setupReference(to flow: Flow)
}

extension FlowChild where Self: SansArgumentInitializable {
    public init(in flow: Flow) {
        self.init()
        setupReference(to: flow)
    }
}

public protocol SpecificFlowCaller: class {
    associatedtype SomeFlow: Flow
    var flow: SomeFlow? { get set }
}

extension Flow {
    public func createChild<FlowType>() -> FlowType where FlowType: FlowChild & SansArgumentInitializable {
        return FlowType(in: self)
    }
    public func setupChildrenReferences() {
        let mirror = Mirror(reflecting: self)
        mirror.children
            .compactMap { $0.value as? FlowChild }
            .forEach { $0.setupReference(to: self) }
    }
}

extension SpecificFlowCaller where Self: FlowChild {
    func setupReference(to flow: Flow) {
        self.flow = flow as? SomeFlow
    }
}

extension Flow {
    func setupReference(to flow: Flow) {
        parent = flow
    }
}

extension ActionPerformerCaller where Self: FlowChild {
    func setupReference(to flow: Flow) {
        actionPerformer = flow
    }
}
