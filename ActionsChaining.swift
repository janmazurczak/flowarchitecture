//
//  ActionsChaining.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation

public extension Action {
    
    static func sequence(_ actions: [Action]) -> ActionsSequence {
        return ActionsSequence(actions)
    }
    
    static func group(_ actions: [Action]) -> ActionsGroup {
        return ActionsGroup(actions)
    }
    
    static func alternative(_ actions: [Action]) -> ActionsAlternativeGroup {
        return ActionsAlternativeGroup(actions)
    }
    
}

open class ActionsSequence: Action, ParentAction {
    //TODO: Results chaining
    public init(_ a: [Action]) {
        actions = a
        super.init()
    }
    public let actions: [Action]
    private var offset: Int = -1
    private var completion: (() -> Void)?
    private func executeNext() {
        offset += 1
        guard offset < actions.count, !isCancelled else {
            completion?()
            return
        }
        actions[offset].prepareExecuteAndFinalise() { [weak self] in
            self?.executeNext()
        }
    }
    public final override func perform(completion: @escaping () -> Void) {
        self.offset = -1
        self.completion = completion
        executeNext()
    }
}

open class ActionsGroup: Action, ParentAction {
    public init(_ a: [Action]) {
        actions = a
        super.init()
    }
    public let actions: [Action]
    public final override func perform(completion: @escaping () -> Void) {
        let group = DispatchGroup()
        var count = 0
        for action in actions {
            group.enter()
            count += 1
            action.prepare()
        }
        for action in actions {
            if action.isCancelled {
                group.leave()
                count -= 1
                continue
            }
            action.perform(completion: { [weak action] in
                action?.finalise()
                group.leave()
                count -= 1
            })
        }
        if count > 0 {
            group.notify(queue: OperationQueue.current?.underlyingQueue ?? .main, execute: completion)
        } else {
            completion()
        }
    }
}

open class ActionsAlternativeGroup: Action, ParentAction {
    public init(_ a: [Action]) {
        actions = a
        super.init()
    }
    public let actions: [Action]
    private var finishedAction: Action? = nil
    private let finishQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    public final override func perform(completion: @escaping () -> Void) {
        finishedAction = nil
        for action in actions {
            action.prepare()
        }
        var performingCount = 0
        for action in actions {
            finishQueue.waitUntilAllOperationsAreFinished()
            if action.isCancelled { continue }
            performingCount += 1
            action.perform(completion: { [weak self, weak action] in
                self?.finishQueue.addOperation {
                    if self?.finishedAction == nil {
                        self?.finishedAction = action
                        for otherAction in (self?.actions ?? []).filter({ $0 !== action }) {
                            otherAction.cancel()
                        }
                    }
                }
                self?.finishQueue.waitUntilAllOperationsAreFinished()
                if let action = action, action === self?.finishedAction {
                    action.finalise()
                    completion()
                }
            })
        }
        if performingCount == 0 { // edge case where all actions were cancelled before execution
            completion()
        }
    }
}
