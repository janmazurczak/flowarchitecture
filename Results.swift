//
//  Results.swift
//
//  Created by Jan Mazurczak on 22/05/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

public protocol AnyResultContainer {
    var anyResult: Any? { get }
}

public protocol AnyResultReceiver {
    func receive(result: Any)
}

public protocol Resulting: Action, AnyResultContainer {
    associatedtype Result
    var result: Result? { get }
}

extension Resulting {
    public var anyResult: Any? { return result }
}

